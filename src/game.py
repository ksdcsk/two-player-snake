#
#
#

import pygame
import views.init
import views.game
import views.result
from oth.viewList import ViewList
from keyboard.keyboard import Keyboard


class Game:
    _debug = False
    _surface_screen = None
    _screen_size = None
    _view_no = None
    _view_list = {}

    def __init__(self, debug=False):
        self._debug = debug

        # Init the pygame
        pygame.init()
        pygame.joystick.init()

        # Hide the mouse
        pygame.mouse.set_visible(False)

        # Create the main surface
        if self._debug:
            self._surface_screen = pygame.display.set_mode((1000, 600), pygame.NOFRAME)
        else:
            self._surface_screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)

        # Get the main surface size
        self._screen_size = self._surface_screen.get_size()

        # Set title for the window
        pygame.display.set_caption("2 players Snake")

        # Init the clock
        self._clock = pygame.time.Clock()

        self._init_view_list()

        #if self._debug:
        #    self.change_view(ViewList.GAME)

    def get_screen_surface(self):
        return self._surface_screen

    def get_size(self):
        return self._screen_size

    def change_view(self, new_view):
        self._view_no = new_view
        self._view_list[new_view].init()

    def get_view(self, view_id):
        return self._view_list[view_id]

    def _init_view_list(self):
        # Set the init screen
        self._view_no = ViewList.INIT

        # Init views
        self._view_list[ViewList.INIT] = views.init.Init(self)
        self._view_list[ViewList.GAME] = views.game.Game(self)
        self._view_list[ViewList.RESULT] = views.result.Result(self)

    def run(self):
        done = False

        while not done:
            # FPS settings
            self._clock.tick(10)

            for event in pygame.event.get():
                Keyboard.process_event(event)

            if Keyboard.key_is_pressed(pygame.K_ESCAPE):
                done = True

            self._view_list[self._view_no].tick()

            # Update the screen
            pygame.display.flip()

        self._stop()

    def _stop(self):
        pygame.quit()