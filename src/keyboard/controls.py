#
#
#

import pygame
from .keyboard import Keyboard
from .key_list import KeyList


class Controls:
    _default_controls = [
        {
            KeyList.KEY_UP: pygame.K_w,
            KeyList.KEY_DOWN: pygame.K_s,
            KeyList.KEY_LEFT: pygame.K_a,
            KeyList.KEY_RIGHT: pygame.K_d,
            KeyList.KEY_FIRE: pygame.K_SPACE
        },
        {
            KeyList.KEY_UP: pygame.K_UP,
            KeyList.KEY_DOWN: pygame.K_DOWN,
            KeyList.KEY_LEFT: pygame.K_LEFT,
            KeyList.KEY_RIGHT: pygame.K_RIGHT,
            KeyList.KEY_FIRE: pygame.K_KP0
        }
    ]

    @staticmethod
    def is_pressed(player_id, key):
        return Keyboard.key_is_pressed(Controls._default_controls[player_id][key])
