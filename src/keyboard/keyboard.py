#
#
#


import pygame


class Keyboard:
    _keys_pressed = {}

    def __init__(self):
        pass

    @staticmethod
    def process_event(ev):
        if ev.type == pygame.KEYDOWN:
            Keyboard._keys_pressed[ev.key] = True
        elif ev.type == pygame.KEYUP:
            if ev.key in Keyboard._keys_pressed.keys():
                Keyboard._keys_pressed[ev.key] = False

    @staticmethod
    def key_is_pressed(key):
        if key in Keyboard._keys_pressed.keys():
            return Keyboard._keys_pressed[key]
        else:
            return False
