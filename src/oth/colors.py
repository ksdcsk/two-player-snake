#
#
#

import numpy


class Colors:
    BLACK = (0, 0, 0)
    BROWN = (88, 21, 21)
    RED = (255, 0, 0)
    BLUE = (0, 0, 255)

    VIEW_INIT_BACKGROUND = (255, 255, 255)
    VIEW_INIT_GAME_NAME = (0, 0, 0)
    VIEW_INIT_PLAYERS_NAME = (87, 81, 84)
    VIEW_INIT_CONTROLLER_NOT_OK = (255, 0, 0)
    VIEW_INIT_CONTROLLER_OK = (0, 194, 0)
    VIEW_INIT_START_BUTTON = (22, 22, 22)

    VIEW_GAME_BACKGROUND = (255, 255, 255)
    VIEW_GAME_SKELETON = (20, 20, 20)
    VIEW_GAME_SNAKE_1 = (81, 133, 252)
    VIEW_GAME_SNAKE_2 = (252, 81, 161)

    VIEW_GAME_APPLE_SIMPLE = (0, 255, 0)
    VIEW_GAME_APPLE_DELICIOUS = (235, 228, 13)
    VIEW_GAME_APPLE_DIABOLIC = (238, 0, 255)

    VIEW_RESULT_GAME_OVER_TEXT = (0, 0, 0)
    VIEW_RESULT_PLAYER_LOST = (255, 0, 0)
    VIEW_RESULT_PLAYER_WON = (0, 194, 0)

    @staticmethod
    def generate_random():
        return numpy.random.randint(0, 255), numpy.random.randint(0, 255), numpy.random.randint(0, 255)