#
#
#

import os
import pygame


class Sound:
    NONE = None

    APPLE_CREATED_DEFAULT = 'apple_sound_1.mp3'

    BULLET_SHOT = 'shot.wav'

    @staticmethod
    def _get_path_to_res():
        return os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + '/'

    @staticmethod
    def play(sound):
        if sound is not None:
            pygame.mixer.Sound(Sound._get_path_to_res() + sound).play()
