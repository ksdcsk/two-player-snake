#
#
#

import pygame
from .base import Object


class Snake(Object):
    MOVE_UP = 0x01
    MOVE_DOWN = 0x02
    MOVE_LEFT = 0x03
    MOVE_RIGHT = 0x04

    _last_direction = None
    _color = None
    _length = 0
    _remain_to_grow = 0
    _god_mode = False
    _can_fire = False
    _fire_expire_time = 0

    def __init__(self, box_l_size, box_phy_size, box_phy_margin, color):
        Object.__init__(self, box_l_size, box_phy_size, box_phy_margin)

        self._color = color

    def start(self, b_list, direction):
        self._box_list = []
        self._box_list = b_list
        self._length = len(b_list)
        self._last_direction = direction

    def move(self, direction=None):
        if direction is None:
            direction = self._last_direction

        # Ignore wrong directions
        if self._last_direction == self.MOVE_UP and direction == self.MOVE_DOWN:
            direction = self.MOVE_UP

        if self._last_direction == self.MOVE_DOWN and direction == self.MOVE_UP:
            direction = self.MOVE_DOWN

        if self._last_direction == self.MOVE_LEFT and direction == self.MOVE_RIGHT:
            direction = self.MOVE_LEFT

        if self._last_direction == self.MOVE_RIGHT and direction == self.MOVE_LEFT:
            direction = self.MOVE_RIGHT

        # Move the snake

        # Remove last box
        if self._remain_to_grow > 0:
            self._remain_to_grow -= 1
        elif self._remain_to_grow < 0:
            for i in range(min(len(self._box_list)-1, abs(self._remain_to_grow)+1)):
                self._box_list.pop(0)

            self._remain_to_grow = 0
        else:
            self._box_list.pop(0)

        def gen_coord(x, y):
            c = [x, y]
            if self._god_mode:
                if c[0] >= self._box_logical_size[0]:
                    c[0] = 0
                elif c[0] < 0:
                    c[0] = self._box_logical_size[0] - 1

                if c[1] >= self._box_logical_size[1]:
                    c[1] = 0
                elif c[1] < 0:
                    c[1] = self._box_logical_size[1] - 1

            return c[0], c[1]

        if direction == self.MOVE_RIGHT:
            self._box_list.append(
                gen_coord(
                    self._box_list[-1][0] + 1,
                    self._box_list[-1][1]
                )
            )
        elif direction == self.MOVE_LEFT:
            self._box_list.append(
                gen_coord(
                    self._box_list[-1][0] - 1,
                    self._box_list[-1][1]
                )
            )
        elif direction == self.MOVE_UP:
            self._box_list.append(
                gen_coord(
                    self._box_list[-1][0],
                    self._box_list[-1][1] - 1
                )
            )
        elif direction == self.MOVE_DOWN:
            self._box_list.append(
                gen_coord(
                    self._box_list[-1][0],
                    self._box_list[-1][1] + 1
                )
            )

        self._last_direction = direction

    def grow(self, grow_by):
        self._remain_to_grow += grow_by

    def draw(self):
        bitmap = pygame.Surface((
            self._box_logical_size[0] * (self._box_physical_margin + self._box_physical_size),  # Width
            self._box_logical_size[1] * (self._box_physical_margin + self._box_physical_size),  # Width
        ), pygame.SRCALPHA)  # Transparent background

        for box in self._box_list:
            pygame.draw.rect(bitmap, self._color, (
                self._box_physical_margin + box[0] * (self._box_physical_margin + self._box_physical_size),  # START X,
                self._box_physical_margin + box[1] * (self._box_physical_margin + self._box_physical_size),  # START Y,
                self._box_physical_size,  # WIDTH,
                self._box_physical_size,  # HEIGHT
            ))

        return bitmap

    def set_god_mode(self, val):
        self._god_mode = val

    def get_last_direction(self):
        return self._last_direction

    def get_color(self):
        return self._color

    def get_first_box(self):
        return self._box_list[-1]

    def is_outside_the_visible_box(self):
        if self._box_list[-1][0] < 0 or self._box_list[-1][0] >= self._box_logical_size[0]:
            return True

        if self._box_list[-1][1] < 0 or self._box_list[-1][1] >= self._box_logical_size[1]:
            return True

        return False
