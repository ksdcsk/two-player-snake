#
#
#

import pygame
from oth.sounds import Sound


class Object:
    _box_logical_size = None  # tuple(width, height)
    _box_physical_size = None  # in px
    _box_physical_margin = None  # in px
    _box_list = None

    def __init__(self, box_l_size, box_phy_size, box_phy_margin):
        self._box_logical_size = box_l_size
        self._box_physical_size = box_phy_size
        self._box_physical_margin = box_phy_margin

    def get_box_list(self):
        return self._box_list




class EatableObject(Object):
    _growth_points = None
    _disappear_in = None  # milisenconds
    _disappear_probability = None
    _creation_time = None
    _apparition_sound = Sound.APPLE_CREATED_DEFAULT

    def __init__(self, *args):
        Object.__init__(self, *args)

        self._creation_time = pygame.time.get_ticks()

        # Play apparition sound
        Sound.play(self._apparition_sound)


    def get_growth_points(self):
        return self._growth_points

    def eat(self, snake_who_ate_me, the_other_snake):
        pass

    def is_valid(self):
        if self._disappear_in is not None:
            if pygame.time.get_ticks() - self._creation_time > self._disappear_in:
                return False

        if self._disappear_probability is not None:
            pass

        return True

