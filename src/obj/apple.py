#
#
#

import pygame
import numpy
from .base import EatableObject
from oth.colors import Colors


class Apple(EatableObject):
    _color = None
    _bitmap = None

    def set_position(self, box):
        self._box_list = [box]

        self._generate_bitmap()

    def draw(self):
        return self._bitmap

    def _generate_bitmap(self):
        self._bitmap = pygame.Surface((
            self._box_logical_size[0] * (self._box_physical_margin + self._box_physical_size),  # Width
            self._box_logical_size[1] * (self._box_physical_margin + self._box_physical_size),  # Width
        ), pygame.SRCALPHA)  # Transparent background

        for box in self._box_list:
            start_x = self._box_physical_margin + box[0] * (self._box_physical_margin + self._box_physical_size)
            start_y = self._box_physical_margin + box[1] * (self._box_physical_margin + self._box_physical_size)

            pygame.draw.circle(self._bitmap, self._color, (
                start_x + 5,  # X,
                start_y + 5,  # Y,
            ), 5)

            pygame.draw.rect(self._bitmap, self._color, (
                start_x + 2, start_y + 1,  # X, Y
                6, 8  # (W, H)
            ))

            pygame.draw.rect(self._bitmap, self._color, (
                start_x, start_y + 6,  # X, Y
                10, 2  # (W, H)
            ))

            pygame.draw.lines(self._bitmap, self._color, True, [
                (start_x + 1, start_y + 8),  # X, Y
                (start_x + 8, start_y + 8)  # (X, Y)
            ])

            pygame.draw.lines(self._bitmap, self._color, True, [
                (start_x + 2, start_y + 9),  # X, Y
                (start_x + 7, start_y + 9)  # (X, Y)
            ])

            pygame.draw.lines(self._bitmap, Colors.BLACK, True, [
                (start_x + 3, start_y + 9),  # X, Y
                (start_x + 6, start_y + 9)  # (X, Y)
            ])

            pygame.draw.lines(self._bitmap, Colors.VIEW_GAME_BACKGROUND, True, [
                (start_x + 4, start_y),  # X, Y
                (start_x + 4, start_y)  # (X, Y)
            ])

            pygame.draw.lines(self._bitmap, Colors.BROWN, True, [
                (start_x + 5, start_y),  # X, Y
                (start_x + 5, start_y)  # (X, Y)
            ])

            pygame.draw.lines(self._bitmap, Colors.BROWN, True, [
                (start_x + 4, start_y + 1),  # X, Y
                (start_x + 4, start_y + 3)  # (X, Y)
            ])




class SimpleApple(Apple):
    _color = Colors.VIEW_GAME_APPLE_SIMPLE
    _growth_points = 1


class DeliciousApple(Apple):
    _color = Colors.VIEW_GAME_APPLE_DELICIOUS
    _growth_points = 5
    _disappear_in = 10000  # 10 sec


class PoisonedApple(Apple):
    _color = Colors.BLACK
    _growth_points = -8
    _disappear_in = 20000  # 20 sec


class RandomApple(Apple):
    _color = Colors.RED
    _disappear_in = 15000  # 15 sec

    def __init__(self, *args):
        Apple.__init__(self, *args)

        self._growth_points = numpy.random.randint(-10, 20)


class MagicApple(Apple):
    _color = Colors.VIEW_GAME_APPLE_DELICIOUS
    _growth_points = 1

    def draw(self):
        self._color = Colors.generate_random()
        self._generate_bitmap()

        return self._bitmap

    def eat(self, snake_who_ate_me, the_other_snake):
        snake_who_ate_me.set_god_mode(True)


class DiabolicApple(Apple):
    _color = Colors.VIEW_GAME_APPLE_DIABOLIC
    _growth_points = 0
    _disappear_in = 7000  # 7 sec

    def eat(self, snake_who_ate_me, the_other_snake):
        the_other_snake.grow(-5)


class SuperMegaApple(Apple):
    _color = Colors.BLUE
    _growth_points = 100
    _disappear_in = 30000  # 30 sec
