#
#
#


class CollisionDetector:
    _first_obj = None
    _second_obj = None

    def __init__(self, first_obj, second_obj):
        self._first_obj, self._second_obj = first_obj, second_obj

    def el_1_collided_el_2(self):
        return self._collision1in2(self._first_obj, self._second_obj)

    def el_2_collided_el_1(self):
        return self._collision1in2(self._second_obj, self._first_obj)

    def el_1_auto_collided(self):
        return self.list_has_duplicates(self._first_obj.get_box_list())

    def el_2_auto_collided(self):
        return self.list_has_duplicates(self._second_obj.get_box_list())

    @staticmethod
    def list_has_duplicates(l):
        return len(l) != len(set(l))

    @staticmethod
    def _collision1in2(el1, el2):
        box_list_1 = el1.get_box_list()
        box_list_2 = el2.get_box_list()

        return box_list_1[-1] in box_list_2
