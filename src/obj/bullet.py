#
#
#

import pygame
from .base import Object
from oth.colors import Colors
from oth.sounds import Sound


class Bullet(Object):
    D_UP = 0x01
    D_DOWN = 0x02
    D_LEFT = 0x03
    D_RIGHT = 0x04

    _position = None
    _direction = None
    _speed = 1
    _color = Colors.BLACK

    def __init__(self, *args):
        Object.__init__(self, *args)

        Sound.play(Sound.BULLET_SHOT)

    def set_initial_position(self, box):
        self._box_list = [box]

    def set_color(self, color):
        self._color = color

    def set_direction(self, direction):
        self._direction = direction

    def move(self):
        if self._direction == self.D_UP:
            self._box_list[0] = (
                self._box_list[0][0],
                self._box_list[0][1] - self._speed
            )
        elif self._direction == self.D_DOWN:
            self._box_list[0] = (
                self._box_list[0][0],
                self._box_list[0][1] + self._speed
            )
        elif self._direction == self.D_LEFT:
            self._box_list[0] = (
                self._box_list[0][0] - self._speed,
                self._box_list[0][1]
            )
        elif self._direction == self.D_RIGHT:
            self._box_list[0] = (
                self._box_list[0][0] + self._speed,
                self._box_list[0][1]
            )

    def draw(self):
        bitmap = pygame.Surface((
            self._box_logical_size[0] * (self._box_physical_margin + self._box_physical_size),  # Width
            self._box_logical_size[1] * (self._box_physical_margin + self._box_physical_size),  # Width
        ), pygame.SRCALPHA)  # Transparent background

        for box in self._box_list:
            pygame.draw.rect(bitmap, self._color, (
                self._box_physical_margin + box[0] * (self._box_physical_margin + self._box_physical_size),  # START X,
                self._box_physical_margin + box[1] * (self._box_physical_margin + self._box_physical_size),  # START Y,
                self._box_physical_size,  # WIDTH,
                self._box_physical_size,  # HEIGHT
            ))

        return bitmap

    def is_outside_the_visible_box(self):
        if self._box_list[0][0] < 0 or self._box_list[0][0] >= self._box_logical_size[0]:
            return True

        if self._box_list[0][1] < 0 or self._box_list[0][1] >= self._box_logical_size[1]:
            return True

        return False
