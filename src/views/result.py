#
#
#

import pygame
from .view import View
from oth.viewList import ViewList
from oth.colors import Colors
from keyboard.keyboard import Keyboard

class Result(View):
    _player_lost = None
    _player_won = None
    _stats_start_height = None
    _counter_instruction_text = 0

    def init(self):
        self._player_lost = self._game.get_view(ViewList.GAME).get_player_who_lost()
        self._player_won = 1 if self._player_lost == 2 else 2

        self._stats_start_height = self._game.get_view(ViewList.GAME).user_info_box_start_height()

    def tick(self):
        game_size = self._game.get_size()

        # Clear the area below the game box
        self._surface_screen.fill(Colors.VIEW_GAME_BACKGROUND, (
            0,  # START X
            self._stats_start_height,  # START Y,
            game_size[0],  # WIDTH
            game_size[1] - self._stats_start_height,  # Height
        ))

        if Keyboard.key_is_pressed(pygame.K_RETURN):
            self._game.change_view(ViewList.GAME)

        # Draw the result
        game_over_el_size = self._draw_game_over()
        self._draw_result_player_1(game_over_el_size)
        self._draw_result_player_2(game_over_el_size)

        self._draw_instruction()

    def _draw_result_player_1(self, center_el_size):
        game_size = self._game.get_size()

        font_ubuntu = pygame.font.Font(self.path_to_file('res/fonts/Ubuntu-B.ttf'), 50)

        if self._player_won == 1:
            bitmap = font_ubuntu.render("Player 1 won!", True, Colors.VIEW_RESULT_PLAYER_WON)
        else:
            bitmap = font_ubuntu.render("Player 1 lost!", True, Colors.VIEW_RESULT_PLAYER_LOST)

        bitmap_size = bitmap.get_size()

        self._surface_screen.blit(bitmap, (
            ((game_size[0] - center_el_size[0]) / 2 - bitmap_size[0]) / 2,  # X
            self._stats_start_height + 40,  # Y
        ))

    def _draw_result_player_2(self, center_el_size):
        game_size = self._game.get_size()

        font_ubuntu = pygame.font.Font(self.path_to_file('res/fonts/Ubuntu-B.ttf'), 50)

        if self._player_won == 2:
            bitmap = font_ubuntu.render("Player 2 won!", True, Colors.VIEW_RESULT_PLAYER_WON)
        else:
            bitmap = font_ubuntu.render("Player 2 lost!", True, Colors.VIEW_RESULT_PLAYER_LOST)

        bitmap_size = bitmap.get_size()

        self._surface_screen.blit(bitmap, (
            ((game_size[0] - center_el_size[0]) / 2 + center_el_size[0] + game_size[0] - bitmap_size[0]) / 2,  # X
            self._stats_start_height + 40,  # Y
        ))

    def _draw_game_over(self):
        game_size = self._game.get_size()

        # Game over text
        font_ubuntu = pygame.font.Font(self.path_to_file('res/fonts/Ubuntu-R.ttf'), 90)

        game_over_bitmap = font_ubuntu.render("GAME OVER", True, Colors.VIEW_RESULT_GAME_OVER_TEXT)
        game_over_bitmap_size = game_over_bitmap.get_size()

        self._surface_screen.blit(game_over_bitmap, (
            (game_size[0] - game_over_bitmap_size[0]) / 2,  # X
            self._stats_start_height + 20,  # Y
        ))

        return game_over_bitmap_size

    def _draw_instruction(self):
        # Make this text appear and disappear
        self._counter_instruction_text += 1
        threshold = 16
        if self._counter_instruction_text < threshold / 2:
            return
        elif self._counter_instruction_text >= threshold:
            self._counter_instruction_text = 0

        game_size = self._game.get_size()

        font_ubuntu = pygame.font.Font(self.path_to_file('res/fonts/Ubuntu-R.ttf'), 30)

        bitmap = font_ubuntu.render('Press <Start> to restart the game.', True, Colors.VIEW_INIT_START_BUTTON)
        bitmap_size = bitmap.get_size()

        self._surface_screen.blit(bitmap, (
            (game_size[0] - bitmap_size[0]) / 2,  # width
            (game_size[1] * 0.9)  # height
        ))