#
#
#


from oth.colors import Colors
from .view import *
from keyboard.keyboard import Keyboard
from oth.viewList import ViewList


class Init(View):
    _counter_instruction_text = 0

    def tick(self):

        # Background color
        self._surface_screen.fill(Colors.VIEW_INIT_BACKGROUND)

        self._draw_name_logo()

        if True:  # Keyboard is ready
            self._draw_player(1, True)
            self._draw_player(2, True)

            self._draw_instruction()

            if Keyboard.key_is_pressed(pygame.K_RETURN):
                self._game.change_view(ViewList.GAME)

    def _draw_name_logo(self):
        game_size = self._game.get_size()
        # Game logo
        logo_bitmap = pygame.image.load(self.path_to_file('res/img/snake.png'))
        logo_size = logo_bitmap.get_size()
        # Game name
        font_ubuntu = pygame.font.Font(self.path_to_file('res/fonts/Ubuntu-R.ttf'), 90)
        game_name_bitmap = font_ubuntu.render('Dual-Player Snake', True,
                                              Colors.VIEW_INIT_GAME_NAME)  # type: pygame.Surface
        game_name_text_size = game_name_bitmap.get_size()
        # Draw the logo and the name
        delimiter_size = 30
        self._surface_screen.blit(logo_bitmap, (
            (game_size[0] - logo_size[0]) / 2,  # width
            (game_size[1] * 0.7 - logo_size[1] - delimiter_size - game_name_text_size[1]) / 2,  # height
        ))
        self._surface_screen.blit(game_name_bitmap, (
            (game_size[0] - game_name_text_size[0]) / 2,  # width
            (game_size[1] * 0.7 - logo_size[1] - delimiter_size - game_name_text_size[1]) / 2 + (
            logo_size[1] + delimiter_size),  # height
        ))

    def _draw_player(self, no, controller_ok):
        game_size = self._game.get_size()

        font_ubuntu = pygame.font.Font(self.path_to_file('res/fonts/Ubuntu-R.ttf'), 50)

        # User name
        user_name_bitmap = font_ubuntu.render('Player ' + str(no), True, Colors.VIEW_INIT_PLAYERS_NAME)
        user_name_size = user_name_bitmap.get_size()

        if no == 1:
            self._surface_screen.blit(user_name_bitmap, (
                (game_size[0] * 0.5 - user_name_size[0]) / 2,   # width
                (game_size[1] * 0.7)  # height
            ))
        else:
            self._surface_screen.blit(user_name_bitmap, (
                (game_size[0] * 0.5 - user_name_size[0]) / 2 + game_size[0] * 0.5,  # width
                (game_size[1] * 0.7)  # height
            ))

        # Controller text
        font_ubuntu = pygame.font.Font(self.path_to_file('res/fonts/Ubuntu-B.ttf'), 25)

        if controller_ok:
            controller_bitmap = font_ubuntu.render('You\'re ready to play!', True, Colors.VIEW_INIT_CONTROLLER_OK)
        else:
            controller_bitmap = font_ubuntu.render('Please connect your controller!', True, Colors.VIEW_INIT_CONTROLLER_NOT_OK)

        controller_bitmap_size = controller_bitmap.get_size()

        delimiter_size = 30

        if no == 1:
            self._surface_screen.blit(controller_bitmap, (
                (game_size[0] * 0.5 - controller_bitmap_size[0]) / 2,  # width
                (game_size[1] * 0.7) + delimiter_size + user_name_size[1]  # height
            ))
        else:
            self._surface_screen.blit(controller_bitmap, (
                (game_size[0] * 0.5 - controller_bitmap_size[0]) / 2 + game_size[0] * 0.5,  # width
                (game_size[1] * 0.7) + delimiter_size + user_name_size[1]  # height
            ))

    def _draw_instruction(self):
        # Make this text appear and disappear
        self._counter_instruction_text += 1
        threshold = 16
        if self._counter_instruction_text < threshold / 2:
            return
        elif self._counter_instruction_text >= threshold:
            self._counter_instruction_text = 0

        game_size = self._game.get_size()

        font_ubuntu = pygame.font.Font(self.path_to_file('res/fonts/Ubuntu-R.ttf'), 30)

        bitmap = font_ubuntu.render('Press <Return> to start the game.', True, Colors.VIEW_INIT_START_BUTTON)
        bitmap_size = bitmap.get_size()

        self._surface_screen.blit(bitmap, (
            (game_size[0] - bitmap_size[0]) / 2,  # width
            (game_size[1] * 0.9)    # height
        ))


##
"""
The description of this view:
--------------------------------------------
|                                          |
|                    [IMAGE]               |
|             Dual Player Snake            |
| 70%                                      |
|                                          |
|                                          |
|                                          |
--------------------------------------------
|        50%         |         50%         |
|                    |                     |
| 30%    Player 1    |       Player 2      |
|                    |                     |
--------------------------------------------
"""
##