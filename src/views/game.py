#
#
#

import pygame
import numpy
from .view import View
from oth.colors import Colors
from obj.snake import Snake
from obj.apple import SimpleApple
from obj.apple import DeliciousApple
from obj.apple import MagicApple
from obj.apple import PoisonedApple
from obj.apple import RandomApple
from obj.apple import DiabolicApple
from obj.apple import SuperMegaApple
from obj.bullet import Bullet
from obj.collisionDetector import CollisionDetector
from keyboard.controls import Controls
from keyboard.key_list import KeyList
from oth.viewList import ViewList


class Game(View):
    _black_rect_coord = None
    _white_rect_coord = None
    _user_info_start_height = None
    _black_rect_coord2 = None
    _play_box = None
    _box_size = 10
    _box_margin = 2
    _snake_list = None
    _apple_list = None
    _used_boxes_list = None
    _player_lost = None
    _bullet_list = None

    def init(self):
        self._player_lost = None
        self._do_all_the_computations()

        self._apple_list = []

        self._snake_list = (
            Snake(self._play_box_logical_size, self._box_size, self._box_margin, Colors.VIEW_GAME_SNAKE_1),
            Snake(self._play_box_logical_size, self._box_size, self._box_margin, Colors.VIEW_GAME_SNAKE_2)
        )

        self._snake_list[0].start([
            (1, 1),
            (1, 2),
            (1, 3),
            (1, 4),
            (1, 5),
            ], Snake.MOVE_DOWN)

        max_box_x = int(self._play_box[2] / (self._box_margin + self._box_size)) - 1
        max_box_y = int(self._play_box[3] / (self._box_margin + self._box_size)) - 1

        self._snake_list[1].start([
            (max_box_x-1, max_box_y - 1),
            (max_box_x-1, max_box_y - 2),
            (max_box_x-1, max_box_y - 3),
            (max_box_x-1, max_box_y - 4),
            (max_box_x-1, max_box_y - 5),
        ], Snake.MOVE_UP)

        self._bullet_list = []

    def tick(self):
        game_over = False
        self._used_boxes_list = []

        self._move_and_control_snakes()
        self._move_bullets()

        # Check if game is not over
        if self._snake_list[0].is_outside_the_visible_box() or self._snake_was_shot(0):
            game_over = True
            self._player_lost = 1

        if self._snake_list[1].is_outside_the_visible_box() or self._snake_was_shot(1):
            game_over = True
            self._player_lost = 2

        # Collision between snakes
        snakesCollisionD = CollisionDetector(*self._snake_list)

        if snakesCollisionD.el_1_collided_el_2() or snakesCollisionD.el_1_auto_collided():
            game_over = True
            self._player_lost = 1
        elif snakesCollisionD.el_2_collided_el_1() or snakesCollisionD.el_2_auto_collided():
            game_over = True
            self._player_lost = 2

        # Eat apples
        self._detect_apples_collision()

        if game_over:
            self._game.change_view(ViewList.RESULT)

            return
        else:
            # Background color
            self._surface_screen.fill(Colors.VIEW_GAME_BACKGROUND)

            self._draw_skeleton()

        for snake in self._snake_list:
            self._used_boxes_list.append(snake.get_box_list())

        self._apple_generate()
        self._apples_verify()

        self._draw_apples()
        self._draw_bullets()
        self._draw_snakes()

    def get_player_who_lost(self):
        return self._player_lost

    def user_info_box_start_height(self):
        return self._user_info_start_height

    def _snake_was_shot(self, snake_no):
        for bullet in self._bullet_list:
            col = CollisionDetector(self._snake_list[snake_no], bullet)

            if col.el_2_collided_el_1():
                return True

        return False

    def _draw_bullets(self):
        for bullet in self._bullet_list:
            self._surface_screen.blit(bullet.draw(), (self._play_box[0], self._play_box[1]))

    def _move_bullets(self):
        for bullet in self._bullet_list:
            bullet.move()

        for bullet_key in range(len(self._bullet_list)):
            if self._bullet_list[bullet_key].is_outside_the_visible_box():
                self._bullet_list.pop(bullet_key)
                break

    def _detect_apples_collision(self):
        for snake in self._snake_list:
            for apple_key in range(len(self._apple_list)):

                collDetector = CollisionDetector(snake, self._apple_list[apple_key])

                if collDetector.el_1_collided_el_2():
                    snake.grow(self._apple_list[apple_key].get_growth_points())
                    self._apple_list[apple_key].eat(snake, self._snake_list[0] if self._snake_list[1] is snake else self._snake_list[1])

                    self._apple_list.pop(apple_key)
                    break

    def _apples_verify(self):
        for apple_key in range(len(self._apple_list)):
            if not self._apple_list[apple_key].is_valid():
                self._apple_list.pop(apple_key)
                break

    def _apple_generate(self):
        def generate(appleTypeClass, probability_one_in):
            if 0 < numpy.random.uniform(0, probability_one_in) < 1:
                while True:
                    g_box = (
                        numpy.random.randint(0, self._play_box_logical_size[0] - 1),
                        numpy.random.randint(0, self._play_box_logical_size[1] - 1)
                    )

                    if g_box not in self._used_boxes_list:
                        apple = appleTypeClass(self._play_box_logical_size, self._box_size, self._box_margin)
                        apple.set_position(g_box)

                        self._apple_list.append(
                            apple
                        )

                        break

        # Simple apple
        simple_apple_count = reduce(
            lambda val, x: val+1 if isinstance(x, SimpleApple) else val+0, self._apple_list, 0
        )

        if simple_apple_count == 0:
            generate(SimpleApple, 1)

        # Other apples
        generate(DeliciousApple, 100)
        generate(MagicApple, 1000)
        generate(PoisonedApple, 100)
        generate(RandomApple, 250)
        generate(DiabolicApple, 400)
        generate(SuperMegaApple, 100)

    def _move_and_control_snakes(self):
        # Move the 2 snakes
        for snake_id in range(2):
            if Controls.is_pressed(snake_id, KeyList.KEY_UP):
                self._snake_list[snake_id].move(Snake.MOVE_UP)
            elif Controls.is_pressed(snake_id, KeyList.KEY_DOWN):
                self._snake_list[snake_id].move(Snake.MOVE_DOWN)
            elif Controls.is_pressed(snake_id, KeyList.KEY_LEFT):
                self._snake_list[snake_id].move(Snake.MOVE_LEFT)
            elif Controls.is_pressed(snake_id, KeyList.KEY_RIGHT):
                self._snake_list[snake_id].move(Snake.MOVE_RIGHT)
            else:
                self._snake_list[snake_id].move()

            if Controls.is_pressed(snake_id, KeyList.KEY_FIRE):
                self._snake_list[snake_id].grow(-1)

                # Add a bullet
                bullet = Bullet(self._play_box_logical_size, self._box_size, self._box_margin)
                bullet.set_direction(
                    {
                        Snake.MOVE_UP: Bullet.D_UP,
                        Snake.MOVE_DOWN: Bullet.D_DOWN,
                        Snake.MOVE_LEFT: Bullet.D_LEFT,
                        Snake.MOVE_RIGHT: Bullet.D_RIGHT
                    }[self._snake_list[snake_id].get_last_direction()]
                )
                bullet.set_color(self._snake_list[snake_id].get_color())
                bullet.set_initial_position(self._snake_list[snake_id].get_first_box())
                bullet.move()

                self._bullet_list.append(bullet)

    def _draw_apples(self):
        for apple in self._apple_list:
            self._surface_screen.blit(apple.draw(), (self._play_box[0], self._play_box[1]))

    def _draw_snakes(self):
        # Draw the 2 snakes
        self._surface_screen.blit(self._snake_list[0].draw(), (self._play_box[0], self._play_box[1]))
        self._surface_screen.blit(self._snake_list[1].draw(), (self._play_box[0], self._play_box[1]))

    def _draw_skeleton(self):
        # Cage
        pygame.draw.rect(self._surface_screen, Colors.VIEW_GAME_SKELETON, self._black_rect_coord)
        pygame.draw.rect(self._surface_screen, Colors.VIEW_GAME_BACKGROUND, self._white_rect_coord)

    def _draw_players_info(self):
        pass

    def _do_all_the_computations(self):
        desired_distance_from_margin = 20
        cage_thickness = 1
        user_delimiter_thickness = 20
        user_block_height = 200

        game_size = self._game.get_size()

        def round_down_by(num, by):
            return num - num%by

        cage_size = (
            round_down_by(game_size[0] - desired_distance_from_margin*2 - cage_thickness*2 - self._box_margin, self._box_size + self._box_margin) + self._box_margin,  # width
            round_down_by(game_size[1] - user_block_height - desired_distance_from_margin * 3 - cage_thickness*2 - self._box_margin, self._box_size + self._box_margin) + self._box_margin  # height
                     )

        self._black_rect_coord = (
            (game_size[0] - cage_size[0] - cage_thickness*2) / 2,  # START - X
            desired_distance_from_margin,  # START - Y
            cage_size[0] + cage_thickness * 2,  # END - X
            cage_size[1] + cage_thickness * 2,  # END - Y
        )

        self._white_rect_coord = (
            (game_size[0] - cage_size[0] - cage_thickness * 2) / 2 + cage_thickness,  # START - X
            desired_distance_from_margin + cage_thickness,  # START - Y
            cage_size[0],  # END - X
            cage_size[1],  # END - Y
        )

        self._play_box = self._white_rect_coord

        self._play_box_logical_size = (
            int(self._play_box[2] / (self._box_margin + self._box_size)),
            int(self._play_box[3] / (self._box_margin + self._box_size))
        )

        self._user_info_start_height = self._black_rect_coord[3] + desired_distance_from_margin*2

        # TODO: I have to draw a delimiter for user stats, at the center of the screen after the cage
