#
#
#

import pygame   # for type hinting
import os


class View:
    _game = None
    _surface_screen = None

    def __init__(self, game):
        self._game = game  # type: Game

        self._surface_screen = self._game.get_screen_surface()  # type: pygame.Surface

    def tick(self):
        pass

    def init(self):
        pass

    @staticmethod
    def path_to_file(file):
        return os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + '/' + file