# Two Player Snake

Full-screen, two player snake written in python using pygame. This game was made for fun and for learning purposes (it's entirely based on OOP using the polymorphism priciple).

## Requirements

* Python 2.7
* Pygame 
* Numpy

## Todo

* [ ] Upgrade to Python 3
* [ ] Add a requirements file
* [ ] Write documentation regarding different apple types
